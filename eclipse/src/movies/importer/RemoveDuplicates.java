//Jackson Kwan 1938023
package movies.importer;
import java.util.*;
public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String source, String dest) {
		super(source, dest, false);
	}
	
	public ArrayList<String> process(ArrayList<String> inputArr){
		ArrayList<String> returnArr = new ArrayList<String>();
		for (int i=0; i<inputArr.size(); i++) {
			if (!returnArr.contains(inputArr.get(i))) {
				returnArr.add(inputArr.get(i));
			}
		}
		return returnArr;
	}
}
